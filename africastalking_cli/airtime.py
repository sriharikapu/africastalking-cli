# standard imports
import csv
import logging
from csv import DictReader

# third-party imports
import africastalking

# local imports
import error.validations as er

logg = logging.getLogger(__name__)


def process_args(argparser):
    argparser.add_argument('-f', '--recipient-file', dest='f', type=str, required=True,
                           help='load recipients from file (one number per line)')


def execute(config, args):
    atm = africastalking.Airtime
    processed_phone_numbers = []

    if args.f is not None:
        logg.debug('reading recipients from file {}'.format(args.f))
        with open(args.f, 'r') as f:
            phone_numbers = [row["phone_number"] for row in DictReader(f)]
            for i in range(len(phone_numbers)):
                phone_number = er.process_phone_number(phone_number=phone_numbers[i], region='KE')
                processed_phone_numbers.append(phone_number)

        with open(args.f, 'r') as f:
            amounts = [row["amount"] for row in DictReader(f)]
            for i in range(len(amounts)):
                er.validate_amount(amounts[i])

        with open(args.f, 'r') as f:
            currency_codes = [row["currency_code"] for row in DictReader(f)]
            for i in range(len(currency_codes)):
                er.validate_currency(currency_codes[i])

    batch = []
    for i in range(len(processed_phone_numbers)):
        code = {
            'phone_number': processed_phone_numbers[i],
            'amount': str(amounts[i]),
            'currency_code': currency_codes[i]
        }
        batch.append(code)

    if len(batch) == len(processed_phone_numbers):
        output = []
        for i in range(len(batch)):
            currency_code = batch[i]['currency_code']
            amount = str(batch[i]['amount'])
            phone = batch[i]['phone_number']
            idempotency_key = 'req-1234'
            res = atm.send(phone_number=phone, amount=amount, currency_code=currency_code,
                           idempotency_key=idempotency_key)
            output.append(res)
        logg.debug(output)

        with open('sent_airtime.csv', 'w') as f:
            write = csv.writer(f, delimiter=',')
            write.writerow(("number", "output"))
            for i in range(len(processed_phone_numbers)):
                write.writerow((processed_phone_numbers[i], output[i]))
        return output